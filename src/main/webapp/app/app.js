var app = angular.module('ngZiriusPoc', [

    // ng related dependency
    'ngRoute',
    'ngTouch',

    // i18n internationalization dependency
    'pascalprecht.translate',

    // Bootstrap
    'ui.bootstrap',

    // UI Grid
    'ui.grid',
    'ui.grid.selection',
    'ui.grid.paging',

    // App Modules
    'ngZiriusPoc.Invoice',
    'ngZiriusPoc.Common'
]);

app.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/views/welcome.html'
            })
    }
]);

app.config(['$translateProvider',
    function($translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix: './app/i18n/locale-',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');
    }
])
