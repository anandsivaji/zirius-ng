invoiceModule.factory('InvoiceService', ['$http',
    function($http) {

        var invoices = [];

        var getMatchedInvoice = function(invoices, invoiceId) {
            var inv;
            for (var i = 0; i < invoices.length; i++) {
                var invoice = invoices[i];
                if (invoice.invoiceId == invoiceId) {
                    inv = invoice;
                }
            }
            return inv;
        };

        return {

            getAllInvoices: function(callback) {

                $http.get('./app/data/OfferedInvoice.json')
                    .success(function(data) {
                        invoices = data.invoices;
                        callback(invoices);
                    })
                    .error(function(err) {
                        console.log(err);
                    });
            },

            getInvoiceById: function(invoiceId, callback) {
                var inv = {};
                if (invoices === 'undefined' || invoices.length === 0) {
                    this.getAllInvoices(function(data) {
                        //invoices = data;
                        callback(getMatchedInvoice(invoices, invoiceId));
                    });
                } else {
                    callback(getMatchedInvoice(invoices, invoiceId));
                }
            }
        }
    }
]);