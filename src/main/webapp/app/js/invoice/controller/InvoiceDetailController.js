invoiceModule.controller('InvoiceDetailController', ['$scope', '$routeParams', 'InvoiceService',
    function($scope, $routeParams, InvoiceService) {
        $scope.invoiceSelected = null;

        $scope.getInvoiceById = function() {
            var invoiceId = $routeParams.invoiceId;
            InvoiceService.getInvoiceById(invoiceId, function(result) {
                $scope.invoiceSelected = result;
            });
        }
    }
]);
