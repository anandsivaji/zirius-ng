invoiceModule.controller('InvoiceController', ['$scope', 'InvoiceService',
    function($scope, InvoiceService) {

        $scope.gridOptions = {
            enableSorting: true,
            enableRowSelection: false,
            enableSelectAll: true,
            enableRowHeaderSelection: false,
            multiSelect: false,
            minRowsToShow: 15,
            pagingPageSizes: [25, 50, 75],
            pagingPageSize: 25,
            columnDefs: [{
                name: 'Offer No',
                field: 'offerNo',
                enableColumnMenu: false,
                width: 75
            }, {
                name: 'Customer Name',
                field: 'customerName',
                enableColumnMenu: false,
                minWidth: 95
            }, {
                name: 'PO No',
                field: 'purchaseOrderNo',
                enableColumnMenu: false,
                width: 90
            }, {
                name: 'Contact Ref',
                field: 'contactReferenceId',
                enableColumnMenu: false,
                width: 100
            }, {
                name: 'Your Ref',
                field: 'yourReferenceId',
                enableColumnMenu: false,
                width: 100
            }, {
                name: 'Invoice Date',
                field: 'invoiceDate',
                enableColumnMenu: false,
                width: 100
            }, {
                name: 'Due Date',
                field: 'invoiceDueDate',
                enableColumnMenu: false,
                width: 100
            }, {
                name: 'Amount',
                field: 'invoiceAmount',
                enableColumnMenu: false,
                width: 75
            }, {
                name: 'Paid Amount',
                field: 'paidAmount',
                enableColumnMenu: false,
                width: 75
            }, {
                name: 'view',
                enableCellEdit: true,
                cellTemplate: '<div id="view-invoice-div" ng-click=\"getExternalScopes().onViewInvoice(row.entity);\"><a href="#/invoicedetail/{{row.entity.invoiceId}}" class="btn btn-xs default btn-editable" style="align:center"><i class="fa fa-search"></i> View</a></div>',
                width: 60
            }],
        }

        $scope.getInvoices = function() {

            InvoiceService.getAllInvoices(function(result) {
                $scope.gridOptions.data = result;
            });
        }
    }
]);
