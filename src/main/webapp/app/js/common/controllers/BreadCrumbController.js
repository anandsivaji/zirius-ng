commonModule.controller('BreadCrumbController', ['$scope', '$rootScope', 'BreadCrumbService',
    function($scope, $rootScope, BreadCrumbService) {

        $scope.breadcrumbs = [];

        $scope.initBreadCrumb = function() {
            $scope.breadcrumbs = BreadCrumbService.addBreadCrumb('#/');
        }

        $rootScope.$on('$locationChangeSuccess', function(event, newUrl, oldUrl) {

            console.log('New URL is = ', newUrl); // http://localhost:3000/#/articles/new
            console.log('Old URL is = ', oldUrl); // http://localhost:3000/#/articles

            var newRelativeUrl = newUrl.substring(newUrl.indexOf('#'));
            $scope.breadcrumbs = BreadCrumbService.addBreadCrumb(newRelativeUrl);
        });

        $scope.initBreadCrumb();
    }
]);
