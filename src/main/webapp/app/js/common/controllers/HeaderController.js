commonModule.controller('HeaderController', ['$rootScope', '$scope', 'UserService', '$translate',
    function($rootScope, $scope, UserService, $translate) {

        $rootScope.menuState = false;
        $scope.user = null;

        $scope.init = function() {
            UserService.getUser(function(response) {
                $scope.user = response;
            });
        };

        $scope.toggleAccordionMenu = function() {
            $rootScope.menuState = !$rootScope.menuState;
        };

        $scope.onLangulageSelecetion = function(selectedLanguage) {
            $translate.use(selectedLanguage);
        }

        $scope.init();
    }
]);
