/*
 Author : Shahm Nattarshah

 AccordionMenuController is used to maintain the state for accordian menu.
*/

commonModule.controller('AccordionMenuController', ['$scope', '$rootScope', 'ConfigService',
    function($scope, $rootScope, ConfigService) {

        $scope.menu = {
            isOpen: true,
            openOneAtATime: true
        };

        $scope.menus = [];

        $scope.init = function() {
            ConfigService.getMenu(function(response) {
                $scope.menus = response;
            });
        };

        $scope.onMenuClicked = function(location) {

            $rootScope.menuState = false;
            window.location.href = location;
        };
    }
]);