commonModule.factory('UserService', ['$http',
    function($http) {

        return {

            getUser: function(callback) {
                $http.get('./app/data/User.json')
                    .success(function(data) {
                        callback(data.user);
                    })
                    .error(function(err) {
                        console.log(err);
                    });
            }
        };
    }
]);
