commonModule.factory('BreadCrumbService', [

    function() {

        var keys = {

            'invoicelist': 'Invoice List',
            'invoicedetail': 'Invoice Detail',
        };

        var breadcrumbs = [];

        return {

            addBreadCrumb: function(url) {

                var key = this.getKey(url);

                this.removeBreadCrumbIfExists(key);

                breadcrumbs.push({
                    'key': key,
                    'link': url
                });

                return breadcrumbs;
            },

            removeBreadCrumbIfExists: function(key) {

                var existIndex = -1;

                for (var i = 0, j = breadcrumbs.length; i < j; i++) {

                    var bc = breadcrumbs[i];
                    if (bc.key === key) {
                        existIndex = i;
                    }
                }

                if (existIndex === -1) {
                    return;
                }

                breadcrumbs.splice(existIndex, breadcrumbs.length - existIndex);
            },

            getKey: function(url) {

                var startIdx = url.indexOf('/') + 1;
                var endIdx = (url.substring(startIdx)).indexOf('/');
                var pathname = (endIdx < startIdx) ? url.substring(startIdx) : url.substr(startIdx, endIdx);
                return keys[pathname] || 'Home';
            }
        };
    }
]);