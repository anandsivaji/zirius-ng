commonModule.factory('ConfigService', ['$http',
    function($http) {

        return {

            getMenu: function(callback) {
                $http.get('./app/data/Menu.json')
                    .success(function(data) {
                        callback(data.menus);
                    })
                    .error(function(err) {
                        console.log(err);
                    });
            }
        };
    }
]);
