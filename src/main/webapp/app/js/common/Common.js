var commonModule = angular.module('ngZiriusPoc.Common', []);

commonModule.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/invoicelist', {
                templateUrl: 'app/views/invoice/invoice-list.html',
                controller: 'InvoiceController'
            })
            .when('/invoicedetail/:invoiceId', {
                templateUrl: 'app/views/invoice/invoice-detail.html',
                controller: 'InvoiceDetailController',
            });
    }
]);
